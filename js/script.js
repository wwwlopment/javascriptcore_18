  $(document).ready(function() {
    var monthes = new Array(
      'Січень',
      'Лютий',
      'Березень',
      'Квітень',
      'Травень',
      'Червень',
      'Липень',
      'Серпень',
      'Вересень',
      'Жовтень',
      'Листопад',
      'Грудень',
    );
    var i = 0;
    setInterval(function() {
      if (i < monthes.length) {
        $('li').eq(i).text(function() {
          return monthes[i];
        });
        i++;
      } else {
        $('li:odd').css('color', 'red');
      }
    }, 1000);
  });
